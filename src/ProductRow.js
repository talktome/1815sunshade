import React from 'react';

export default class ProductRow extends React.Component {
  render() {
    return (
      <tr>
        <td align="right">{this.props.product.id}</td>
        <td>{this.props.product.name}</td>
        <td>{this.props.product.type}</td>
        <td>{this.props.product.winch}</td>
        <td align="right">{this.props.product.cassette}</td>
        <td align="right">{this.props.product.tube}</td>
        <td align="right">{this.props.product.front}</td>
        <td align="right">{this.props.product.weight}</td>
        <td align="right">{this.props.product.weave}</td>
        <td align="right">{this.props.product.carriage}</td>
        <td align="right">{this.props.product.note}</td>
      </tr>
    );
  }
}
