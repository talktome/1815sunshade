import React from 'react';
import './Input.css';

export default class Input extends React.Component {
  change(e) {
    this.props.change(e);
  }
  render() {
    return (
      <div>
        <h2>Input</h2>
        <table>
          <tbody>
            <tr>
              <td> Order:</td>
              <td><input type="number" min="1" max="999999" name="order" onChange={this.props.change.bind(this)} /></td>
            </tr>
            <tr>
              <td>Quantity:</td>
              <td><input type="number" min="1" max="99" name="quantity" onChange={this.props.change.bind(this)} /></td>
            </tr>
            <tr>
              <td>Id:</td>
              <td><input type="number" min="11" max="99" name="id" onChange={this.props.change.bind(this)} /></td>
            </tr>
            <tr>
              <td>Width:</td>
              <td><input type="number" min="1" max="999" name="width" onChange={this.props.change.bind(this)} /></td>
            </tr>
            <tr>
              <td>Arm:</td>
              <td><input type="number" min="1" max="999" name="arm" onChange={this.props.change.bind(this)} /></td>
            </tr>
            <tr>
              <td>Colour:</td>
              <td><input type="number" min="1" max="999999" name="color" onChange={this.props.change.bind(this)} /></td>
            </tr>
            <tr>
              <td>Coat:</td>
              <td><input type="number" min="1" max="9" name="coat" onChange={this.props.change.bind(this)} /></td>
			   </tr>
				<p/>
            <tr>
              <td>Cassette</td>
              <td><input type="number" min="200" name="cassette" onChange={this.props.change.bind(this)} /></td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
