import React from 'react';
import ProductRow from './ProductRow.js';

export default class Inventory extends React.Component {
  render() {
    return (
      <div>
        <h2>Inventory</h2>
        <table>
          <thead>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Type</th>
              <th>Winch</th>
              <th>Cassette</th>
              <th>Tube</th>
              <th>Front</th>
              <th>Weight</th>
              <th>Weave</th>
              <th>Carriage</th>
            </tr>
          </thead>
          <tbody>
				 {this.props.selection.map(p => <ProductRow key={p.id} product={p}/>)}
          </tbody>
        </table>
      </div>
    );
  }
}
