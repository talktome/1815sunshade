import React from 'react';

export default class Order extends React.Component {
	render() {
		if (this.props.selection.length !== 1) {
			return <div/>;
		}

		let order;
		if (this.props.input.order) {
			order = <div>Order: {this.props.input.order}</div>;
		}
		else {
			order = <div>Order</div>
		}
		let quantity;
		if (!this.props.input.quantity.length) {
			quantity = 1;
		}
		else {
			quantity = this.props.input.quantity;
		}
		let product = this.props.selection[0];
		let cassette;
		if (product.cassette) {
			//cassette = this.props.input.cassette === '' ? '' : this.props.input.cassette - product.cassette;
			cassette = this.props.input.cassette - product.cassette >= 100 ? this.props.input.cassette - product.cassette : '';
			cassette = <div>Cassette: {cassette}</div>;
		}
		let tube;
		if (product.tube) {
			tube = <div>Tube: {1300 - product.tube}</div>
		}
		let front;
		if (product.front) {
			front = <div>Front: {1300 - product.front}</div>
		}
		let width = this.props.input.width - product.weave;
		if (!(width > 0)) {
			width = 0;
		}
		let pieces;
		if (width <= 1140) {
			pieces = 1;
		}
		else if (width <= 2310) {
			pieces = 2;
		}
		else if (width <= 3480) {
			pieces = 3;
		}
		else if (width <= 4650) {
			pieces = 4;
		}
		else if (width <= 5820) {
			pieces = 5;
		}
		let length;
		if (product.type === "awning") {
			length = this.props.input.arm * 2 + 300;
		}
		else if (product.type === "terrace") {
			length = this.props.input.arm + 500;
		}
		let unitLength = pieces * length;
		let totalLength = quantity * unitLength;

		quantity = <div>Quantity: {quantity}</div>
		if (pieces) {
			pieces = <div>Pieces/unit: {pieces}</div>
		}
		else {
			pieces = <div/>
		}

		width = <div>Width: {width}</div>
		length = <div>Lenght: {length}</div>
		unitLength = <div>Length/unit: {unitLength}</div>;
		totalLength = <div>Total length: {totalLength}</div>;
		return (
			<div>
				<h2>{order}</h2>
				{quantity}
				{cassette}
				{tube}
				{front}
				{pieces}
				{width}
				{length}
				{unitLength}
				{totalLength}
			</div>
		)
	}
}
